# Blotto Game

Web-based environment for playing the [Blotto game](https://en.wikipedia.org/wiki/Blotto_game).

It was written for an educational roadtrip, where random people could enter they army sizes and the engine evaluated the results of games with all previous players. There is a duel mode, too.

It is meant to be played on a tablet or a mobile device.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

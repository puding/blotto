module.exports = {
    publicPath: process.env.NODE_ENV === 'production' ? '/blotto/' : '/',
    productionSourceMap: false,
    pwa: {
        name: 'blotto',
        themeColor: '#FFFFFF',
        msTileColor: '#FFFFFF',
    },
}

export default {
    soldiers: 100,
    battlefields: 5,
    masters: [
        {
            email: 'puding',
            armies: [20, 20, 20, 20, 20],
        },
        {
            email: 'bohdan',
            armies: [10, 20, 30, 40, 0],
        },
        {
            email: 'kebab',
            armies: [0, 0, 0, 0, 0],
        },
        {
            email: 'lietadlo',
            armies: [100, 100, 100, 100, 100],
        },
    ],
    localStorageKey: 'blotto',
}

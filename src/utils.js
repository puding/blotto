export default {
    evaluateOneVsOne(first, second) {
        let battlesWon = 0
        for (let i = 0; i < first.length; i++) {
            if (first[i] > second[i]) {
                battlesWon++
            }
        }
        return battlesWon
    },
    evaluateOneVsMany(one, many) {
        let warsWon = 0
        for (let rival of many) {
            if (this.evaluateOneVsOne(one, rival) > one.length / 2) {
                warsWon++
            }
        }
        return warsWon
    },
}

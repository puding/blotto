import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Admin from './views/Admin.vue'

Vue.use(Router)

export default new Router({
    mode: 'hash',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/index.html',
            name: 'index',
            component: Home,
        },
        {
            path: '/',
            name: 'home',
            component: Home,
        },
        {
            path: '/admin',
            name: 'admin',
            component: Admin,
        },
    ],
})
